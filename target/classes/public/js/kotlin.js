setInterval(updateView, 3000);

function updateView() {
    $.get('/api/accounts', function (data) {

        var table = $('#bot-table');
        var count = table.find('tr').length - 1;
        var parsed = JSON.parse(data);

        //Database has more entries than displayed on page.
        if (count < parsed.length) {

            $.get('/api/accounts/last/appendable',function(data) {
                table.append(data);
            })

        } else {

            parsed.forEach(function (b) {

                var prefix = b['Username'];


                var password = document.getElementById(prefix + "_password").textContent;
                var world = document.getElementById(prefix + "_world").textContent;

                if(password == null || world == null) {
                    return;
                }

                if (password != b['Password']) {
                    document.getElementById(prefix + "_password").textContent = b['Password'];
                }

                if (world != b['World']) {
                    document.getElementById(prefix + "_world").textContent = b['World'];
                }
            });
        }
    });
}

