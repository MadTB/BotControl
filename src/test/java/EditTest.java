import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Random;

/**
 * Created by Mad on 12/18/2015.
 */
public class EditTest {

    public static String generateString(Random rng, String characters, int length)
    {
        char[] text = new char[length];
        for (int i = 0; i < length; i++)
        {
            text[i] = characters.charAt(rng.nextInt(characters.length()));
        }
        return new String(text);
    }


    public static void main(String[] args) throws IOException, InterruptedException {

        String chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".toLowerCase();
        String urlBefore = "http://localhost:4567/api/account/";

        for (int i = 0; i < 100; i++) {
            URL url = new URL(urlBefore + "xietrxl/" + generateString(new Random(), chars, 7) + "/382");
            System.out.println(url);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("PUT");
            int responseCode = connection.getResponseCode();
            System.out.println(responseCode);
            Thread.sleep(3000);
        }
    }
}
