package backend

import backend.routes.API
import spark.Spark.staticFileLocation


/**
* Created by Mad on 12/16/2015.
*/
fun main(args: Array<String>) {

    staticFileLocation("/public");
    API();
    println("BotControlServer has been initialized.");

}

