package backend.database

import com.mongodb.MongoClient
import com.mongodb.client.model.Filters
import com.mongodb.client.model.UpdateOptions
import org.bson.Document

/**
 * Created by Mad on 12/16/2015.
 */
object Database {

    private val client = MongoClient();
    private val bc = client.getDatabase("BotControl");

    private val bots = bc.getCollection("Bots");

    fun updateDocument(id: String, newDocument: Document): Unit {
        bots.updateOne(Document("_id", id), Document("$" + "set", newDocument), UpdateOptions().upsert(true));
    }

    public class Bots {

        data class BotAccount(val username: String, val password: String, val world: String);

        fun getAllAccounts(): List<BotAccount> {
            val bots = listOf<BotAccount>().toArrayList();
            Database.bots.find(Filters.and(Filters.exists("Username"), Filters.exists("World")))
                    .forEach { x ->
                        bots.add(BotAccount(x.getString("Username"), x.getString("Password"), x.getString("World")))
                    }
            return bots;
        }

        fun getAllAccountsAsJSON(): List<String> {
                return Database.bots.find(
                        Filters.and(Filters.exists("Username"), Filters.exists("World"))).map { x -> x.toJson() };
        }

        fun getLastAccount() : BotAccount {
            val document = Database.bots.find(
                    Filters.and(Filters.exists("Username"), Filters.exists("World"))).last();

            return BotAccount(
                    document.getString("Username"),
                    document.getString("Password"),
                    document.getString("World"));
        }

        fun getLastAccountAsJSON(): String {
            return Database.bots.find(
                    Filters.and(Filters.exists("Username"), Filters.exists("World"))).last().toJson();
        }

    }
}
