package backend.routes

import backend.database.Database
import kotlinx.html.*
import kotlinx.html.stream.appendHTML

/**
 * Created by Mad on 12/17/2015.
 */
class Test() {

    fun render(): String {

        fun append() = StringBuilder().appendHTML();

        return append().html {

            head {
                link { rel = "stylesheet"; href = "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" }
                script { src = "//code.jquery.com/jquery-1.11.3.min.js" }
                script { src = ".././js/kotlin.js" }
                script { src = "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" }
            }

            body {

                div(classes = "container") {

                    nav(classes = "navbar navbar-default")

                    table(classes = "table table-bordered") { id = "bot-table"

                        tr {
                            th { +"Username" }
                            th { +"Password" }
                            th { +"World" }
                        }

                        Database.Bots().getAllAccounts().forEach { x ->
                            tr {
                                td { id = x.username + "_username"; + x.username };
                                td { id = x.username + "_password"; + x.password };
                                td { id = x.username + "_world"; + x.world };
                            }
                        }

                    }

                }

            }

        }.toString()
    }
}


/*
        return html {






                + "<link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css/>"

            script { attribute("src", "//code.jquery.com/jquery-1.11.3.min.js") }
            script { attribute("src", ".././js/kotlin.js") }
            script { attribute("src", "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js") }

            table {

                tr {
                    th { +"Username" }
                    th { +"Password" }
                    th { +"World" }
                }

                Database.getAllAccounts().forEach { x ->
                    val json = Gson().fromJson(x, JsonObject::class.java);
                    val username = json.get("Username").asString;
                    tr {
                        td { attribute("id", username + "_username");+username };
                        td { attribute("id", username + "_password");+json.get("Password").asString };
                        td { attribute("id", username + "_world");+json.get("World").asString };
                    }

                }
            }

        }
    }
    */






