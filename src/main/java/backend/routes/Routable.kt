package backend.routes

import spark.Request
import spark.Response
import spark.Spark

interface Routable {

    val baseURL: String

    fun get(path: String, action: (Request, Response) -> String) {
        val pathWithBase = baseURL + path;
        return Spark.get(pathWithBase, action);
    }

    fun post(path: String, action: (Request, Response) -> String) {
        val pathWithBase = baseURL + path;
        return Spark.post(pathWithBase, action);
    }

    fun put(path: String, action: (Request, Response) -> String) {
        val pathWithBase = baseURL + path;
        return Spark.put(pathWithBase, action);
    }

}
