package backend.routes

import backend.database.Database
import backend.database.schema.Bot
import frontend.util.templates.AccountAsAppendable
import org.bson.Document

/**
 * Created by mad on 12/15/15.
 */
class API : Routable {

    var requestMap = emptyMap<String, Pair<Int, Long>>().toSortedMap();

    override val baseURL: String
        get() = "/api";

    init {
        put("/account/:name/:password/:world", { req, res ->
            val requests = requestMap[req.ip()];

            if(requests == null) {
                requestMap.put(req.ip(), Pair(1, System.currentTimeMillis()));
            } else {
                requestMap.put(req.ip(), Pair(requests.first + 1, System.currentTimeMillis()));
            }
            println(requestMap);

            val doc = Document(
                    hashMapOf(
                            Pair(Bot._id, req.params(":name")),
                            Pair(Bot.username, req.params(":name")),
                            Pair(Bot.password, req.params(":password")),
                            Pair(Bot.world, req.params(":world"))) as Map<String, Any>);

            if(requests !=null) {

                val time = requests.second;
                val amount = requests.first;

                if(amount < 10) {
                    Database.updateDocument(req.params("name"), doc);

                } else if(System.currentTimeMillis() >= time + 100) {
                    Database.updateDocument(req.params("name"), doc);
                    requestMap.put(req.ip(), Pair(1, System.currentTimeMillis()));

                } else {
                    println("Too fast!");
                }

            } else {
                Database.updateDocument(req.params("name"), doc);
            }


            "Account added successfully!";
        });

        get("/accounts", { req, res ->
            Database.Bots().getAllAccountsAsJSON().toString();
        });

        get("/accounts/last", {req, res ->
            Database.Bots().getLastAccountAsJSON();
        });

        get("/accounts/last/appendable", {req, res ->
            AccountAsAppendable().render();
        });


        get("/test", {req , res -> Test().render()})
    }
}

