package frontend.util.templates

import backend.database.Database
import kotlinx.html.stream.appendHTML
import kotlinx.html.td
import kotlinx.html.tr

/**
 * Created by Mad on 12/17/2015.
 */
class AccountAsAppendable {

    fun render(): String {

        fun append() = StringBuilder().appendHTML();
        val ba = Database.Bots().getLastAccount();

        return append().tr {
            id = ba.username;
            td { id = ba.username + "_username"; + ba.username };
            td { id = ba.username + "_password"; + ba.password };
            td { id = ba.username + "_world"; + ba.world };

        }.toString();

    }

}